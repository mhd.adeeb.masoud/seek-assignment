package com.adeeb.seek.assignment.ui.theme

import androidx.compose.ui.graphics.Color

val DarkBlue = Color(0xFF0d3880)
val LightBlue = Color (0xFF1d559d)
val DarkPink = Color(0xFFE60278)
val Orange = Color(0xFFFD8E39)