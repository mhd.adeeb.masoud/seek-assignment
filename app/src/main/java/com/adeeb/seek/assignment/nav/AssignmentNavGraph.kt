package com.adeeb.seek.assignment.nav

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.adeeb.seek.assignment.ui.addpost.AddJobPostScreen
import com.adeeb.seek.assignment.ui.common.ConsumeResultOnce
import com.adeeb.seek.assignment.ui.common.finishWithResult
import com.adeeb.seek.assignment.ui.jobposts.JobPostsScreen
import com.adeeb.seek.assignment.ui.login.LoginScreen

private const val RESULT_KEY_ADD_SUCCESSFUL = "AddSuccessful"

fun NavGraphBuilder.setupAssignmentNavGraph(
    navController: NavHostController,
    logoutCallback: () -> Unit
) {
    composable(LOGIN_SCREEN) {
        LoginScreen(navigateToJobPosts = {
            Screen.JobPostsScreen().navigate(navController)
        })
    }

    composable(JOB_POSTS_SCREEN) {
        JobPostsScreen(
            navigateToAddPost = { Screen.AddJobPostScreen.navigate(navController) },
            logoutCallback = logoutCallback,
            onAddResult = { resultCallBack ->
                ConsumeResultOnce(
                    navController,
                    RESULT_KEY_ADD_SUCCESSFUL,
                    false
                ) { result -> resultCallBack(result) }
            }
        )
    }

    composable(ADD_JOB_POST_SCREEN) {
        AddJobPostScreen(navigateBack = { navController.popBackStack() }) {
            finishWithResult(navController, RESULT_KEY_ADD_SUCCESSFUL, true)
        }
    }
}