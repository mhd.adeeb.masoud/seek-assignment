package com.adeeb.seek.assignment

import android.app.Application
import com.facebook.soloader.SoLoader
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SeekApp : Application() {

    override fun onCreate() {
        super.onCreate()
        SoLoader.init(this, false)
    }
}