package com.adeeb.seek.assignment.ui.login

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.adeeb.seek.assignment.R
import com.adeeb.seek.assignment.ui.common.SeekTextField
import com.adeeb.seek.assignment.ui.common.SnackBarErrorHandler
import com.adeeb.seek.assignment.ui.common.TextFieldType

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun LoginScreen(
    modifier: Modifier = Modifier,
    loginViewModel: LoginViewModel = hiltViewModel(),
    navigateToJobPosts: () -> Unit = {}
) {
    val scaffoldState = rememberScaffoldState()
    val uiState by loginViewModel.uiState.collectAsState()
    val coroutineScope = rememberCoroutineScope()

    //error
    SnackBarErrorHandler(
        uiState.error,
        scaffoldState,
        coroutineScope,
        loginViewModel::onErrorConsumed
    )

    Scaffold(scaffoldState = scaffoldState, topBar = {
        TopAppBar(
            modifier = Modifier.fillMaxWidth(),
            title = { Text(text = stringResource(R.string.title_sign_in)) },
            contentColor = Color.White,
            backgroundColor = MaterialTheme.colors.primary
        )
    }) {

        //navigation
        if (uiState.shouldNavigateToJobPostsScreen) {
            navigateToJobPosts()
            loginViewModel.onNavigateToJobPostsScreen()
        }
        Column(modifier = modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
            SeekTextField(
                modifier = Modifier.padding(top = 16.dp),
                label = stringResource(R.string.label_email),
                value = uiState.email,
                isError = !uiState.isEmailValid,
                onValueChanged = loginViewModel::onEmailChanged,
                textFieldType = TextFieldType.EMAIL
            )
            SeekTextField(
                modifier = Modifier.padding(top = 16.dp),
                label = stringResource(R.string.label_password),
                value = uiState.password,
                isError = !uiState.isPasswordValid,
                onValueChanged = loginViewModel::onPasswordChanged,
                textFieldType = TextFieldType.PASSWORD,
                imeAction = ImeAction.Done
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 48.dp),
                horizontalArrangement = Arrangement.Center,
            ) {
                if (!uiState.isLoggingIn) {
                    val keyboardController = LocalSoftwareKeyboardController.current
                    Button(
                        modifier = Modifier
                            .width(150.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .animateContentSize(),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = MaterialTheme.colors.secondary,
                            contentColor = Color.White
                        ),
                        enabled = uiState.enableLoginButton,
                        onClick = {
                            loginViewModel.onLoginClicked()
                            keyboardController?.hide()
                        }
                    ) {
                        Text(
                            modifier = Modifier.padding(4.dp),
                            text = stringResource(R.string.button_login)
                        )
                    }
                } else
                    CircularProgressIndicator()
            }
        }
    }
}