package com.adeeb.seek.assignment.network

import com.adeeb.seek.assignment.data.Location
import retrofit2.http.GET

interface LocationsApi {
    @GET("/locations")
    suspend fun getLocations() : List<Location>
}