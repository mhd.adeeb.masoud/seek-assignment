package com.adeeb.seek.assignment.usecase

import com.adeeb.seek.assignment.data.JobPost
import com.adeeb.seek.assignment.network.JobPostsApi
import com.adeeb.seek.assignment.storage.KeyValueStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface JobPostsUseCase {
    val companyName: String
    suspend fun getJobPosts(): List<JobPost>
}

class JobPostsUseCaseImpl(private val jobPostsApi: JobPostsApi, private val keyValueStorage: KeyValueStorage) : JobPostsUseCase {

    override val companyName: String get() = keyValueStorage.getCompanyName()

    override suspend fun getJobPosts(): List<JobPost> {
        withContext(Dispatchers.IO) {
            Thread.sleep(3000)
        }
        return jobPostsApi.getJobPosts()
    }
}