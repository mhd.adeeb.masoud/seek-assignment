package com.adeeb.seek.assignment.usecase

import com.adeeb.seek.assignment.network.JobPostRequestBody
import com.adeeb.seek.assignment.network.JobPostsApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface AddJobPostUseCase {
    suspend fun addJobPost(
        title: String,
        description: String,
        status: String,
        salaryMin: Int,
        salaryMax: Int,
        locationIds: List<Int> = listOf()
    )
}

class AddJobPostUseCaseImpl(private val jobPostsApi: JobPostsApi) : AddJobPostUseCase {
    override suspend fun addJobPost(
        title: String,
        description: String,
        status: String,
        salaryMin: Int,
        salaryMax: Int,
        locationIds: List<Int>
    ) {
        withContext(Dispatchers.IO) {
            Thread.sleep(3000)
        }

        jobPostsApi.addJobPost(
            JobPostRequestBody(
                title,
                description,
                listOf(salaryMin, salaryMax),
                locationIds,
                status
            )
        )
    }

}