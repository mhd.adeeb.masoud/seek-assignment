package com.adeeb.seek.assignment.ui.common

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.navigation.NavController
import androidx.navigation.NavHostController

@Composable
fun <T> ConsumeResultOnce(
    navController: NavController,
    resultKey: String,
    defaultValue: T,
    onResultCallBack: (T) -> Unit
) {
    navController.currentBackStackEntry
        ?.savedStateHandle?.getStateFlow(resultKey, defaultValue)
        ?.collectAsState()?.value?.let {
            onResultCallBack(it)
            navController.currentBackStackEntry
                ?.savedStateHandle
                ?.remove<T>(resultKey)
        }
}


fun <T> finishWithResult(navController: NavHostController, resultKey: String, result: T) {
    navController.popBackStack()
    navController.currentBackStackEntry
        ?.savedStateHandle?.set<T>(resultKey, result)
}