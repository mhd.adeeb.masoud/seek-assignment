package com.adeeb.seek.assignment.nav

import androidx.navigation.NavController
import androidx.navigation.NavOptions

const val LOGIN_SCREEN = "Login"
const val JOB_POSTS_SCREEN = "JobPosts"
const val ADD_JOB_POST_SCREEN = "AddJobPost"

sealed class Screen(
    val screenName: String,
    private val navigateCallback: (NavController) -> Unit = { it.navigate(screenName) }
) {
    fun navigate(navController: NavController) {
        navigateCallback(navController)
    }

    object LoginScreen : Screen(LOGIN_SCREEN)
    class JobPostsScreen : Screen(
        JOB_POSTS_SCREEN,
        {
            it.navigate(
                JOB_POSTS_SCREEN,
                NavOptions.Builder().setPopUpTo(LoginScreen.screenName, inclusive = true).build()
            )
        })

    object AddJobPostScreen : Screen(ADD_JOB_POST_SCREEN)
}
