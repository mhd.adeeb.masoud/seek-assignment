package com.adeeb.seek.assignment.di

import com.adeeb.seek.assignment.BuildConfig
import com.adeeb.seek.assignment.network.AuthInterceptor
import com.adeeb.seek.assignment.network.JobPostsApi
import com.adeeb.seek.assignment.network.LocationsApi
import com.adeeb.seek.assignment.network.LoginApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
@InstallIn(ViewModelComponent::class)
object NetworkModule {

    @Provides
    fun provideOkHttpClientBuilder(): OkHttpClient.Builder = OkHttpClient
        .Builder()
        .connectTimeout(5, TimeUnit.SECONDS)
        .readTimeout(5, TimeUnit.SECONDS)
        .callTimeout(5, TimeUnit.SECONDS)
        .writeTimeout(5, TimeUnit.SECONDS)
        .retryOnConnectionFailure(false)

    @Provides
    fun provideRetrofitBuilder(): Retrofit.Builder =
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())

    @Provides
    @Named("NoAuth")
    fun provideOkHttpClient(builder: OkHttpClient.Builder): OkHttpClient = builder.build()

    @Provides
    @Named("Auth")
    fun provideAuthOkHttpClient(builder: OkHttpClient.Builder,authInterceptor: AuthInterceptor): OkHttpClient =
        builder
            .addInterceptor(authInterceptor)
            .build()

    @Provides
    @Named("Auth")
    fun provideAuthRetrofit(
        builder: Retrofit.Builder,
        @Named("Auth") client: OkHttpClient
    ): Retrofit =
        builder
            .client(client)
            .build()

    @Provides
    @Named("NoAuth")
    fun provideRetrofit(
        builder: Retrofit.Builder,
        @Named("NoAuth") client: OkHttpClient
    ): Retrofit =
        builder
            .client(client)
            .build()

    @Provides
    fun provideLoginApi(@Named("NoAuth") retrofit: Retrofit): LoginApi =
        retrofit.create(LoginApi::class.java)

    @Provides
    fun provideJobPostsApi(@Named("Auth") retrofit: Retrofit): JobPostsApi =
        retrofit.create(JobPostsApi::class.java)

    @Provides
    fun provideLocationsApi(@Named("Auth") retrofit: Retrofit): LocationsApi =
        retrofit.create(LocationsApi::class.java)
}