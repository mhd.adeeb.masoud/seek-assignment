package com.adeeb.seek.assignment.ui.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.adeeb.seek.assignment.data.Location

@Composable
fun LocationDialog(
    dismissCallback: () -> Unit,
    locations: List<Location>,
    selectedLocations: SnapshotStateList<Location>
) {
    Dialog(onDismissRequest = { dismissCallback() }) {
        Surface(modifier = Modifier.fillMaxWidth(), elevation = 12.dp) {
            Column(modifier = Modifier.padding(16.dp), verticalArrangement = Arrangement.spacedBy(8.dp)) {
                LazyColumn {
                    items(locations, key = { it.id }) { location ->
                        Row(modifier = Modifier.fillMaxWidth()) {
                            LabelledCheckBox(
                                label = location.name,
                                checked = selectedLocations.contains(location),
                                onCheckedChange = {
                                    if (selectedLocations.contains(location))
                                        selectedLocations.remove(location)
                                    else
                                        selectedLocations.add(location)
                                })
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun LabelledCheckBox(
    checked: Boolean,
    onCheckedChange: ((Boolean) -> Unit),
    label: String,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .clip(MaterialTheme.shapes.small)
            .clickable(
                indication = rememberRipple(color = MaterialTheme.colors.primary),
                interactionSource = remember { MutableInteractionSource() },
                onClick = { onCheckedChange(!checked) }
            )
            .requiredHeight(ButtonDefaults.MinHeight)
            .padding(4.dp)
    ) {
        Checkbox(
            checked = checked,
            onCheckedChange = null
        )

        Spacer(Modifier.size(6.dp))

        Text(
            text = label,
        )
    }
}