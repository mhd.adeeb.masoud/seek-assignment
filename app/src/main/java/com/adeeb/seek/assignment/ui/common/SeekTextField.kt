package com.adeeb.seek.assignment.ui.common

import android.view.KeyEvent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SeekTextField(
    modifier: Modifier = Modifier,
    label: String,
    placeholder: String? = null,
    value: String = "",
    isError: Boolean = false,
    onValueChanged: (String) -> Unit = {},
    imeAction: ImeAction = ImeAction.Next,
    textFieldType: TextFieldType = TextFieldType.TEXT,
    enabled: Boolean = true
) {
    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current
    Row(
        modifier = Modifier
            .padding(horizontal = 32.dp)
            .fillMaxWidth()
        , horizontalArrangement = Arrangement.Center
    ) {
        TextField(
            value = value,
            enabled = enabled,
            modifier = modifier
                .widthIn(320.dp, 500.dp)
                .fillMaxWidth()
                .onPreviewKeyEvent {
                    if (it.key == Key.Tab && it.nativeKeyEvent.action == KeyEvent.ACTION_DOWN) {
                        focusManager.moveFocus(FocusDirection.Down)
                        true
                    } else {
                        false
                    }
                },
            onValueChange = {
                if (it.isEmpty() || textFieldType != TextFieldType.NUMBER || it.toUIntOrNull() != null) {
                    onValueChanged(it)
                }
            },
            singleLine = textFieldType != TextFieldType.TEXT_MULTILINE,
            label = { Text(text = label) },
            visualTransformation = when (textFieldType) {
                TextFieldType.PASSWORD -> PasswordVisualTransformation()
                else -> VisualTransformation.None
            },
            keyboardOptions = KeyboardOptions(
                imeAction = imeAction,
                keyboardType = when (textFieldType) {
                    TextFieldType.PASSWORD -> KeyboardType.Password
                    TextFieldType.EMAIL -> KeyboardType.Email
                    TextFieldType.NUMBER -> KeyboardType.Number
                    else -> KeyboardType.Text
                }
            ),
            keyboardActions = KeyboardActions(
                onNext = { focusManager.moveFocus(FocusDirection.Down) },
                onDone = {keyboardController?.hide()}
            ),
            placeholder = placeholder?.let {
                {
                    Text(text = placeholder)
                }
            },
            isError = isError,
        )
    }
}

enum class TextFieldType { EMAIL, PASSWORD, TEXT, NUMBER, TEXT_MULTILINE }