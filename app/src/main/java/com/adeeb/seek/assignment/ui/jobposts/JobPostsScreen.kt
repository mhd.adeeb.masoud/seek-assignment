package com.adeeb.seek.assignment.ui.jobposts

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Logout
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.adeeb.seek.assignment.R
import com.adeeb.seek.assignment.data.JobPost
import com.adeeb.seek.assignment.data.JobPostStatus
import com.adeeb.seek.assignment.data.Location
import com.adeeb.seek.assignment.ui.common.SnackBarErrorHandler
import com.adeeb.seek.assignment.ui.theme.Orange
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@Composable
fun JobPostsScreen(
    modifier: Modifier = Modifier,
    joPostsViewModel: JobPostsViewModel = hiltViewModel(),
    navigateToAddPost: () -> Unit = {},
    logoutCallback: () -> Unit = {},
    onAddResult: @Composable ((Boolean) -> Unit) -> Unit = {}
) {

    val uiState by joPostsViewModel.jobListingsUiState.collectAsState()
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()
    //error
    SnackBarErrorHandler(
        uiState.error,
        scaffoldState,
        coroutineScope,
        joPostsViewModel::onErrorConsumed
    )
    //refresh on result
    onAddResult { addSuccessful -> if (addSuccessful) joPostsViewModel.reloadJobListings() }

    Scaffold(modifier = modifier.fillMaxSize(), scaffoldState = scaffoldState, topBar = {
        TopAppBar(
            modifier = Modifier.fillMaxWidth(),
            title = { Text(text = stringResource(R.string.title_job_posts)) },
            actions = {
                IconButton(onClick = { logoutCallback() }) {
                    Icon(
                        imageVector = Icons.Filled.Logout,
                        contentDescription = stringResource(R.string.content_desc_logout),
                        tint = Color.White
                    )
                }
            },
            contentColor = Color.White,
            backgroundColor = MaterialTheme.colors.primary
        )
    }, floatingActionButton = {
        FloatingActionButton(
            modifier = Modifier.padding(bottom = 32.dp, end = 8.dp),
            contentColor = Color.White,
            backgroundColor = MaterialTheme.colors.secondary,
            onClick = { navigateToAddPost() }) {
            Icon(
                imageVector = Icons.Filled.Add,
                contentDescription = stringResource(R.string.content_desc_add_post),
                modifier = Modifier.size(32.dp),

                )
        }
    }) {
        SwipeRefresh(
            modifier = modifier.padding(top = it.calculateTopPadding()),
            state = rememberSwipeRefreshState(isRefreshing = uiState.isLoading),
            onRefresh = joPostsViewModel::reloadJobListings,
            swipeEnabled = true
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .apply {
                        if (uiState.jobPosts.isEmpty())
                            verticalScroll(rememberScrollState())
                    }
            ) {
                if (uiState.jobPosts.isEmpty() && !uiState.isLoading) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier.weight(weight = 1f, fill = true)
                    ) {
                        Text(
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.Center,
                            text = stringResource(R.string.text_empty_job_posts)
                        )
                    }
                } else {
                    LazyColumn(
                        modifier = Modifier.weight(weight = 1f, fill = true),
                        contentPadding = PaddingValues(vertical = 8.dp),
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                        content = {
                            items(items = uiState.jobPosts, key = JobPost::id) { jobPost ->
                                JobPostItem(
                                    jobPost.positionTitle,
                                    jobPost.status,
                                    jobPost.jobDescription,
                                    jobPost.locations.map(Location::name).joinToString(", "),
                                    jobPost.postingDate.substring(0, 10),
                                    "$" + jobPost.salary.replace(",", " - $")
                                )
                            }
                        })
                }
            }
        }
    }

}

@Composable
fun JobPostItem(
    jobTitle: String = "",
    status: String = "",
    jobDescription: String = "",
    locations: String = "",
    postedAt: String = "",
    salary: String = ""
) {
    Surface(
        elevation = 2.dp,
        modifier = Modifier
            .padding(horizontal = 8.dp)
            .fillMaxWidth()
    ) {
        Column(modifier = Modifier.padding(8.dp)) {
            Text(
                text = jobTitle,
                style = MaterialTheme.typography.h6.copy(fontWeight = FontWeight.Bold)
            )
            Text(
                text = salary,
                style = MaterialTheme.typography.caption,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = status,
                style = MaterialTheme.typography.subtitle1,
                color = when (JobPostStatus.getStatus(status)) {
                    JobPostStatus.OPEN -> Color.Green
                    JobPostStatus.CLOSED -> Color.Red
                    JobPostStatus.PENDING -> Orange
                }
            )
            Text(text = locations, style = MaterialTheme.typography.subtitle2)
            Text(
                modifier = Modifier.alpha(0.7f),
                text = jobDescription,
                style = MaterialTheme.typography.caption,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
            Text(text = postedAt, style = MaterialTheme.typography.overline)
        }
    }
}
