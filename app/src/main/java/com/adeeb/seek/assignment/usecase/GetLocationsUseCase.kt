package com.adeeb.seek.assignment.usecase

import com.adeeb.seek.assignment.data.Location
import com.adeeb.seek.assignment.network.LocationsApi

interface GetLocationsUseCase {
    suspend fun getLocations() : List<Location>
}

class GetLocationsUseCaseImpl(private val locationsApi: LocationsApi) : GetLocationsUseCase {
    override suspend fun getLocations(): List<Location> = locationsApi.getLocations()
}