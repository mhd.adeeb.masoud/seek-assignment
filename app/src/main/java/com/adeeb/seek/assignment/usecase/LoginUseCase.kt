package com.adeeb.seek.assignment.usecase

import com.adeeb.seek.assignment.network.LoginApi
import com.adeeb.seek.assignment.network.LoginRequestBody
import com.adeeb.seek.assignment.storage.KeyValueStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface LoginUseCase {
    suspend fun login(email: String, password: String): String
}

class LoginUseCaseImpl (private val loginApi: LoginApi, private val keyValueStorage: KeyValueStorage) : LoginUseCase {

    override suspend fun login(email: String, password: String): String {
        withContext(Dispatchers.IO) {
            Thread.sleep(3000)
        }
        val response = loginApi.login(LoginRequestBody(email, password))
        keyValueStorage.saveToken(response.token)
        keyValueStorage.saveCompanyName(response.user)
        return response.user
    }

}