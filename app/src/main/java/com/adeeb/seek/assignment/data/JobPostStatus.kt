package com.adeeb.seek.assignment.data

enum class JobPostStatus(private val statusName: String) {
    OPEN("open"),
    CLOSED("closed"),
    PENDING("pending");

    companion object {
        val statusList: List<String>
            get() = values().map(JobPostStatus::statusName)

        fun getStatus(name: String) = values().firstOrNull { it.statusName == name } ?: OPEN
    }
}