package com.adeeb.seek.assignment.network

import com.adeeb.seek.assignment.storage.KeyValueStorage
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(private val keyValueStorage: KeyValueStorage) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val token = keyValueStorage.getToken()
        if (token.isNullOrBlank()) {
            chain.proceed(originalRequest)
        }
        val requestBuilder = originalRequest.newBuilder()
            .header("Authorization", "Bearer $token")
        val request = requestBuilder.build()
        return chain.proceed(request)
    }

}
