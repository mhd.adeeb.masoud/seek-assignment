package com.adeeb.seek.assignment

import androidx.lifecycle.ViewModel
import com.adeeb.seek.assignment.storage.KeyValueStorage
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val keyValueStorage: KeyValueStorage) : ViewModel() {
    fun logout() {
        keyValueStorage.saveToken(null)
    }
//    private val _shouldRedirectToMainPage = MutableStateFlow(false)
//    val shouldRedirectToMainPage = _shouldRedirectToMainPage.asStateFlow()

    val shouldRedirect: Boolean get() = keyValueStorage.getToken()?.isNotBlank() == true

//    init {
//        viewModelScope.launch(Dispatchers.IO) {
//            _shouldRedirectToMainPage.value = keyValueStorage.getToken()?.isNotBlank() == true
//        }
//    }
}