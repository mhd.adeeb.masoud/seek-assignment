package com.adeeb.seek.assignment.network

import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {
    @POST("/login")
    suspend fun login(@Body loginInfo : LoginRequestBody) : LoginResponse
}