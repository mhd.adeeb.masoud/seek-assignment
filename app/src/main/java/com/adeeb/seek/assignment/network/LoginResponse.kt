package com.adeeb.seek.assignment.network

import com.google.gson.annotations.SerializedName

data class LoginResponse (
    @SerializedName("token")
    val token: String = "",
    @SerializedName("user")
    val user: String = ""
)