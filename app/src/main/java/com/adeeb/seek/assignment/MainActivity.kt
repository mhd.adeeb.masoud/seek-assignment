package com.adeeb.seek.assignment

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavOptions
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.adeeb.seek.assignment.nav.JOB_POSTS_SCREEN
import com.adeeb.seek.assignment.nav.LOGIN_SCREEN
import com.adeeb.seek.assignment.nav.Screen
import com.adeeb.seek.assignment.nav.setupAssignmentNavGraph
import com.adeeb.seek.assignment.ui.theme.SeekAssignmentTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val mainViewModel: MainViewModel = hiltViewModel()
            var shouldRedirect by rememberSaveable { mutableStateOf(mainViewModel.shouldRedirect) }
            SeekAssignmentTheme {
                val navController = rememberNavController()
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    NavHost(
                        navController = navController,
                        startDestination = LOGIN_SCREEN
                    ) {
                        setupAssignmentNavGraph(navController) {
                            mainViewModel.logout()
                            navController.navigate(
                                LOGIN_SCREEN,
                                NavOptions.Builder()
                                    .setPopUpTo(JOB_POSTS_SCREEN, inclusive = true)
                                    .build()
                            )
                        }
                    }
                    if (shouldRedirect) {
                        Screen.JobPostsScreen().navigate(navController)
                        shouldRedirect = false
                    }
                }
            }
        }
    }
}