package com.adeeb.seek.assignment.ui.common

import androidx.annotation.StringRes
import androidx.compose.material.ScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun SnackBarErrorHandler(
    @StringRes error: Int?,
    scaffoldState: ScaffoldState,
    coroutineScope: CoroutineScope,
    errorConsumedCallback: () -> Unit
) {
    error?.let {
        val stringError = stringResource(it)
        coroutineScope.launch {
            scaffoldState.snackbarHostState.showSnackbar(stringError)
        }
        errorConsumedCallback()
    }
}