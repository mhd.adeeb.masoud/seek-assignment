package com.adeeb.seek.assignment.data

import com.google.gson.annotations.SerializedName

data class JobPost(
    @SerializedName("id")
    val id: Int,
    @SerializedName("positionTitle")
    val positionTitle: String,
    @SerializedName("jobDescription")
    val jobDescription: String,
    @SerializedName("postingDate")
    val postingDate: String,
    @SerializedName("salary")
    val salary: String,
    @SerializedName("locations")
    val locations: List<Location>,
    @SerializedName("status")
    val status: String
)