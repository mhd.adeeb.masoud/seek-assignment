package com.adeeb.seek.assignment.ui.addpost

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adeeb.seek.assignment.R
import com.adeeb.seek.assignment.data.Location
import com.adeeb.seek.assignment.usecase.AddJobPostUseCase
import com.adeeb.seek.assignment.usecase.GetLocationsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddJobPostViewModel @Inject constructor(
    private val addJobPostUseCase: AddJobPostUseCase,
    private val getLocationsUseCase: GetLocationsUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow(AddJobPostUiState())
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            var retryCount = 0
            while (uiState.value.locations == null && retryCount++ < 5) {
                try {
                    val locations = getLocationsUseCase.getLocations()
                    _uiState.value = _uiState.value.copy(locations = locations)
                } catch (_: Exception) {
                }
            }
            if (uiState.value.locations == null) {
                _uiState.value = _uiState.value.copy(error = R.string.error_loading_locations)
            }
        }
    }

    fun onSaveClicked(
        status: String,
        title: String,
        salaryMin: String,
        salaryMax: String,
        jobDescription: String,
        selectedLocations: List<Location>
    ) {
        if (status.isBlank() || title.isBlank() || salaryMin.isBlank() || salaryMax.isBlank() || jobDescription.isBlank()) {
            _uiState.value = _uiState.value.copy(error = R.string.error_invalid_form)
            return
        }
        if (selectedLocations.isEmpty()) {
            _uiState.value = _uiState.value.copy(error = R.string.error_select_location)
            return
        }
        val salaryMinInt = salaryMin.toInt()
        val salaryMaxInt = salaryMax.toInt()
        if (salaryMinInt > salaryMaxInt) {
            _uiState.value =
                _uiState.value.copy(error = R.string.error_min_max_salary)
            return
        }
        _uiState.value = _uiState.value.copy(isSaving = true)
        viewModelScope.launch {
            try {
                addJobPostUseCase.addJobPost(
                    title,
                    jobDescription,
                    status,
                    salaryMinInt,
                    salaryMaxInt,
                    selectedLocations.map(Location::id)
                )
                _uiState.value = _uiState.value.copy(shouldNavigateBackWithResult = true)
            } catch (ex: Exception) {
                _uiState.value = _uiState.value.copy(error = R.string.generic_error, isSaving = false)
            }
        }
    }

    fun onErrorConsumed() {
        _uiState.value = _uiState.value.copy(error = null)
    }

    fun onNavigateBack() {
        _uiState.value = _uiState.value.copy(shouldNavigateBackWithResult = false)
    }
}

data class AddJobPostUiState(
    val isSaving: Boolean = false,
    val shouldNavigateBackWithResult: Boolean = false,
    val locations: List<Location>? = null,
    val selectedLocations: HashSet<Location> = HashSet(),
    @StringRes val error: Int? = null
)