package com.adeeb.seek.assignment.ui.addpost

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.adeeb.seek.assignment.R
import com.adeeb.seek.assignment.data.JobPostStatus
import com.adeeb.seek.assignment.data.Location
import com.adeeb.seek.assignment.ui.common.LocationDialog
import com.adeeb.seek.assignment.ui.common.SeekTextField
import com.adeeb.seek.assignment.ui.common.SnackBarErrorHandler
import com.adeeb.seek.assignment.ui.common.TextFieldType

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddJobPostScreen(
    modifier: Modifier = Modifier,
    addJobPostViewModel: AddJobPostViewModel = hiltViewModel(),
    navigateBack: () -> Unit,
    navigateBackWithPositiveResult: () -> Unit = {}
) {
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()
    val uiState by addJobPostViewModel.uiState.collectAsState()
    //error
    SnackBarErrorHandler(
        uiState.error,
        scaffoldState,
        coroutineScope,
        addJobPostViewModel::onErrorConsumed
    )
    //navigate back
    if (uiState.shouldNavigateBackWithResult) {
        navigateBackWithPositiveResult()
        addJobPostViewModel.onNavigateBack()
    }
    //locations dialog
    var showAlertDialog by rememberSaveable {
        mutableStateOf(false)
    }
    val checkedLocations = remember {
        mutableStateListOf<Location>()
    }

    if (uiState.locations != null && showAlertDialog) {
        LocationDialog(
            dismissCallback = { showAlertDialog = false },
            locations = uiState.locations!!,
            selectedLocations = checkedLocations
        )
    }

    Scaffold(modifier = modifier.fillMaxSize(),
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(R.string.title_add_job_post)) },
                navigationIcon = {
                    IconButton(onClick = { navigateBack() }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = stringResource(R.string.content_desc_back)
                        )
                    }
                },
                contentColor = Color.White,
                backgroundColor = MaterialTheme.colors.primary
            )
        }) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxHeight()
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {

            var titleText by rememberSaveable { mutableStateOf("") }
            var salaryMinText by rememberSaveable { mutableStateOf("") }
            var salaryMaxText by rememberSaveable { mutableStateOf("") }
            var jobDescriptionText by rememberSaveable { mutableStateOf("") }

            val radioOptions = JobPostStatus.statusList
            val (selectedOption, onOptionSelected) = rememberSaveable { mutableStateOf(radioOptions[0]) }

            StatusRadioButtons(radioOptions, selectedOption, onOptionSelected)
            SeekTextField(
                label = stringResource(R.string.label_job_title),
                value = titleText,
                onValueChanged = { titleText = it })
            SeekTextField(
                label = stringResource(R.string.label_min_salary),
                value = salaryMinText,
                onValueChanged = { salaryMinText = it },
                textFieldType = TextFieldType.NUMBER
            )
            SeekTextField(
                label = stringResource(R.string.label_max_salary),
                value = salaryMaxText,
                onValueChanged = { salaryMaxText = it },
                textFieldType = TextFieldType.NUMBER
            )
            SeekTextField(
                modifier = Modifier.clickable {
                    if (uiState.locations != null)
                        showAlertDialog = true
                },
                label = stringResource(R.string.label_locations),
                value = checkedLocations.map(Location::name).joinToString(", "),
                enabled = false
            )
            SeekTextField(
                modifier = Modifier.height(200.dp), label = stringResource(R.string.label_job_desc),
                value = jobDescriptionText,
                onValueChanged = { jobDescriptionText = it },
                imeAction = ImeAction.Default,
                textFieldType = TextFieldType.TEXT_MULTILINE
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 48.dp),
                horizontalArrangement = Arrangement.Center,
            ) {
                if (!uiState.isSaving) {
                    val keyboardController = LocalSoftwareKeyboardController.current
                    Button(
                        modifier = Modifier
                            .width(150.dp)
                            .clip(MaterialTheme.shapes.medium)
                            .animateContentSize(),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = MaterialTheme.colors.secondary,
                            contentColor = Color.White
                        ),
                        onClick = {
                            addJobPostViewModel.onSaveClicked(
                                status = selectedOption,
                                title = titleText,
                                salaryMin = salaryMinText,
                                salaryMax = salaryMaxText,
                                jobDescription = jobDescriptionText,
                                selectedLocations = checkedLocations
                            )
                            keyboardController?.hide()
                        }
                    ) {
                        Text(modifier = Modifier.padding(4.dp), text = stringResource(R.string.button_save))
                    }
                } else
                    CircularProgressIndicator()
            }
            Spacer(
                modifier = Modifier
                    .height(48.dp)
                    .fillMaxWidth()
            )
        }
    }
}

@Composable
fun StatusRadioButtons(
    radioOptions: List<String>,
    selectedOption: String,
    onOptionSelected: (String) -> Unit
) {
    Row(horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()) {
        radioOptions.forEachIndexed { index, text ->
            Row(
                Modifier
                    .selectable(
                        selected = (text == selectedOption),
                        onClick = {
                            onOptionSelected(text)
                        }
                    )
                    .padding(end = if (index == radioOptions.lastIndex) 0.dp else 32.dp)
            ) {
                RadioButton(
                    selected = (text == selectedOption),
                    onClick = { onOptionSelected(text) }
                )
                Text(
                    text = text,
                    style = MaterialTheme.typography.body1.merge(),
                    modifier = Modifier.padding(top = 10.dp)
                )
            }
        }
    }
}