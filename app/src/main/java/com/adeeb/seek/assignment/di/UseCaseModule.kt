package com.adeeb.seek.assignment.di

import com.adeeb.seek.assignment.network.JobPostsApi
import com.adeeb.seek.assignment.network.LocationsApi
import com.adeeb.seek.assignment.network.LoginApi
import com.adeeb.seek.assignment.storage.KeyValueStorage
import com.adeeb.seek.assignment.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {
    @Provides
    fun provideLoginUseCase(loginApi: LoginApi, keyValueStorage: KeyValueStorage): LoginUseCase =
        LoginUseCaseImpl(loginApi, keyValueStorage)

    @Provides
    fun provideJobPostsUseCase(jobPostsApi: JobPostsApi, keyValueStorage: KeyValueStorage): JobPostsUseCase =
        JobPostsUseCaseImpl(jobPostsApi, keyValueStorage)

    @Provides
    fun provideAddJobPostUseCase(jobPostsApi: JobPostsApi): AddJobPostUseCase =
        AddJobPostUseCaseImpl(jobPostsApi)

    @Provides
    fun provideGetLocationsUseCase(locationsApi: LocationsApi): GetLocationsUseCase =
        GetLocationsUseCaseImpl(locationsApi)
}