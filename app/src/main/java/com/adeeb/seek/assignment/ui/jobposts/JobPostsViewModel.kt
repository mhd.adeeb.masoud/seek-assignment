package com.adeeb.seek.assignment.ui.jobposts

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adeeb.seek.assignment.R
import com.adeeb.seek.assignment.data.JobPost
import com.adeeb.seek.assignment.usecase.JobPostsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class JobPostsViewModel @Inject constructor(private val jobPostsUseCase: JobPostsUseCase) :
    ViewModel() {
    private val _jobListingsUiState = MutableStateFlow(JobListingsUiState())
    val jobListingsUiState = _jobListingsUiState.asStateFlow()

    init {
        _jobListingsUiState.value =
            _jobListingsUiState.value.copy(companyName = jobPostsUseCase.companyName)
        reloadJobListings()
    }

    fun reloadJobListings() {
        _jobListingsUiState.value = _jobListingsUiState.value.copy(isLoading = true)
        viewModelScope.launch {
            try {
                val jobPosts = jobPostsUseCase.getJobPosts()
                _jobListingsUiState.value = _jobListingsUiState.value.copy(jobPosts = jobPosts)
            } catch (ex: Exception) {
                _jobListingsUiState.value =
                    _jobListingsUiState.value.copy(error = R.string.generic_error)
            } finally {
                _jobListingsUiState.value = _jobListingsUiState.value.copy(isLoading = false)
            }
        }
    }

    fun onErrorConsumed() {
        _jobListingsUiState.value = _jobListingsUiState.value.copy(error = null)
    }
}

data class JobListingsUiState(
    val jobPosts: List<JobPost> = listOf(),
    val companyName: String = "",
    val isLoading: Boolean = false,
    @StringRes val error: Int? = null
)