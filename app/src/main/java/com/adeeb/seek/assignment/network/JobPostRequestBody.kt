package com.adeeb.seek.assignment.network

import com.google.gson.annotations.SerializedName

data class JobPostRequestBody (
    @SerializedName("positionTitle")
    val positionTitle: String,
    @SerializedName("jobDescription")
    val jobDescription: String,
    @SerializedName("salary")
    val salary: List<Int>,
    @SerializedName("locationIds")
    val locationsIds: List<Int>,
    @SerializedName("status")
    val status: String
)