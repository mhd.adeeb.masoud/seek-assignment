package com.adeeb.seek.assignment.di

import android.content.Context
import com.adeeb.seek.assignment.storage.KeyValueStorage
import com.adeeb.seek.assignment.storage.KeyValueStorageImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object SecureStorageModule {
    @Provides
    fun provideSecureStorage(@ApplicationContext context: Context): KeyValueStorage =
        KeyValueStorageImpl(context)
}