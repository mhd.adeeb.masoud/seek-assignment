package com.adeeb.seek.assignment.storage

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.util.Base64
import android.util.Base64.DEFAULT
import com.facebook.android.crypto.keychain.AndroidConceal
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain
import com.facebook.crypto.CryptoConfig
import com.facebook.crypto.Entity
import com.facebook.crypto.keychain.KeyChain


interface KeyValueStorage {
    fun saveToken(token: String?)
    fun getToken(): String?
    fun saveCompanyName(companyName: String)
    fun getCompanyName(): String
}

class KeyValueStorageImpl(context: Context) : KeyValueStorage {
    private val prefs = context.getSharedPreferences("SECURE", MODE_PRIVATE)
    private val keyChain: KeyChain = SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256)
    private val crypto = AndroidConceal.get().createDefaultCrypto(keyChain)

    override fun saveToken(token: String?) {
        val tokenToSave = token?.let {
            val cipherText: ByteArray = crypto.encrypt(token.toByteArray(), Entity.create("token"))
            Base64.encodeToString(cipherText, DEFAULT)
        }
        prefs.edit().putString("token", tokenToSave).apply()
    }

    override fun getToken(): String? {
        val encToken = prefs.getString("token", "")
        if (encToken.isNullOrBlank()) return null
        val tokenByteArray =
            crypto.decrypt(Base64.decode(encToken, DEFAULT), Entity.create("token"))
        return String(tokenByteArray)
    }

    override fun saveCompanyName(companyName: String) {
        prefs.edit().putString("companyName", companyName).apply()
    }

    override fun getCompanyName(): String {
        return prefs.getString("companyName", "")!!
    }
}