package com.adeeb.seek.assignment.ui.login

import android.util.Patterns
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adeeb.seek.assignment.R
import com.adeeb.seek.assignment.usecase.LoginUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val loginUseCase: LoginUseCase) : ViewModel() {

    private val _uiState = MutableStateFlow(LoginUiState())
    val uiState = _uiState.asStateFlow()

    fun onLoginClicked() {
        viewModelScope.launch {
            try {
                _uiState.value = uiState.value.copy(isLoggingIn = true)
                loginUseCase.login(uiState.value.email, uiState.value.password)
                _uiState.value = uiState.value.copy(shouldNavigateToJobPostsScreen = true)
            } catch (exception: Exception) {
                _uiState.value = uiState.value.copy(error = R.string.generic_error)
                _uiState.value = uiState.value.copy(isLoggingIn = false)
            }
        }
    }

    fun onNavigateToJobPostsScreen() {
        _uiState.value = uiState.value.copy(shouldNavigateToJobPostsScreen = false)
    }

    fun onEmailChanged(email: String) {
        _uiState.value = uiState.value.copy(
            email = email,
            isEmailValid = Patterns.EMAIL_ADDRESS.matcher(email).matches()
        )
    }

    fun onPasswordChanged(password: String) {
        _uiState.value =
            uiState.value.copy(
                password = password,
                isPasswordValid = password.length > 5
            )
    }

    fun onErrorConsumed() {
        _uiState.value = uiState.value.copy(error = null)
    }

    data class LoginUiState(
        val isLoggingIn: Boolean = false,
        val isEmailValid: Boolean = true,
        val isPasswordValid: Boolean = true,
        val shouldNavigateToJobPostsScreen: Boolean = false,
        @StringRes val error: Int? = null,
        val email: String = "",
        val password: String = ""
    ) {
        val enableLoginButton: Boolean
            get() {
                return !isLoggingIn &&
                        isEmailValid &&
                        isPasswordValid &&
                        email.isNotBlank() &&
                        password.isNotBlank()
            }
    }
}