package com.adeeb.seek.assignment.network

import com.adeeb.seek.assignment.data.JobPost
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface JobPostsApi {
    @GET("/jobPosts")
    suspend fun getJobPosts() : List<JobPost>

    @POST("/JobPost")
    suspend fun addJobPost(@Body jobPostRequest: JobPostRequestBody)
}