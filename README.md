# Seek Android Assignment
This is an implementation of Seek Assignment on the Android platform.
## Tech Stack
- Kotlin programming language
- CLEAN project architecture
- Jetpack Compose & Material Design for the UI layer
- MVVM & StateFlow for the presentation layer
- Kotlin Coroutines for asynchronous code handling
- Hilt for dependency injection
- Retrofit & OkHttp for networking
- Facebook Conceal for Token Encryption
- Google GSON for JSON parsing

## Features
- Login
- List job posts
- Add job post
- Retrieve job post locations
- Supports Material Design theming
- Supports landscape and portrait orientations
- Supports dark and light modes

## Before Building
Make sure to change the base server URL to your local node instance.
Change BASE_URL in `app/build.gradle` and also in `app/src/main/res/xml/network_security_config` (if you will be using HTTP).

### Default Deployment
The server is deployed to https://adeeb-seek-assignment1.glitch.me/
You need to open https://adeeb-seek-assignment1.glitch.me/locations to wake up the server and make sure it displays locations in json as follows:

```
[{"id":4,"name":"Berlin"},{"id":3,"name":"Damascus"},{"id":1,"name":"Kuala Lumpur"},{"id":5,"name":"Singapore"},{"id":2,"name":"Sydney"}]
```

Use email: `admin@test.com` and password `admin123` to login
